import argparse

def get_args_compute_impulse():
    parser = argparse.ArgumentParser(description='Calcula resposta acústica ao impulso da sala especificada')
    parser.add_argument('env', help='geometria do ambiente (arquivo .obj)')
    parser.add_argument('envparams', help='parâmetros do ambiente (arquivo .json)')
    parser.add_argument('--drawenv', action='store_const', const=True, default=False, help='desenha o ambiente')
    parser.add_argument('--plotresponse', action='store_const', const=True, default=False, help='plota as respostas ao impulso geradas')
    parser.add_argument('--spark', action='store_const', const=True, default=False, help='spark mode')
    parser.add_argument('--numworkers', default='*', help='número de workers usados pelo spark')
    parser.add_argument('--numrays', default=1, type=int, help='número de raios a serem emitidos por cada emissor')

    args = parser.parse_args()

    args_dict = {
        "env_geometry_file": args.env,
        "env_params_file": args.envparams,
        "draw_env": args.drawenv,
        "plot_response": args.plotresponse,
        "spark_mode": args.spark,
        "num_workers": args.numworkers,
        "num_rays": args.numrays
    }

    return args_dict
