import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

#triangulo

p0x = 0
p0y = 0
p0z = 0

p1x = 0
p1y = -1
p1z = 1

p2x = 0
p2y = 1
p2z = 0

ux = p1x - p0x
uy = p1y - p0y
uz = p1z - p0z

vx = p2x - p0x
vy = p2y - p0y
vz = p2z - p0z

#reta

wx = 1
wy = 0
wz = 0

px = -2
py = -1
pz = 1.1

###

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

triangles =  [
((p0x,p0y,p0z),(p1x,p1y,p1z),(p2x,p2y,p2z))
]

ax.add_collection3d(Poly3DCollection(triangles, linewidths=1, color='0.5'))
ax.plot3D([px,px+wx*10],[py,py+wy*10],[pz,pz+wz*10], linewidth=1)

ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
ax.set_zlim(-1,1)

##

V = np.array([[ux, vx, -wx], [uy, vy, -wy], [uz, vz, -wz]])
P = np.array([[px - p0x], [py - p0y], [pz - p0z]])

K = np.dot(np.linalg.inv(V), P)

print(K)


if K[0][0] >= 0 and K[1][0] >= 0 and (K[0][0] + K[1][0] <= 1) and K[2][0] >= 0:
    ax.plot3D([px+wx*K[2][0]],[py+wy*K[2][0]],[pz+wz*K[2][0]], 'ro')
else:
    print('no intersection')


plt.show()

