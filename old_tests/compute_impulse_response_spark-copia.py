import numpy as np
import matplotlib.pyplot as plt
from queue import Queue
import random
import sys

from objects import Ray, Triangle, Wall, Receiver, Transmitter, Environment
from utils import normalize, reflect, cut_impulse_response, draw_ray, draw_triangle, plot_impulse_response
import constants

import pyspark

def func(f):
    return lambda args: f(*args)
     

def nearest_intersection(ray, triangles):

    # procura a interseção mais proxima, entre o raio e os triangulos na lista

    min_dist = np.inf
    nearest_triangle = None

    for triangle in triangles:
        intersec = triangle.calc_intersection(ray)
        if intersec is not None: 
            intersec_w = intersec[2]
            if (intersec_w < min_dist) and (intersec_w > constants.min_intersection_dist): # evita raio colidir na parede em que foi gerado
                min_dist = intersec_w
                nearest_triangle = triangle

    return ray, nearest_triangle, min_dist


def interact_triangle(ray, triangle, intersec_w):

    obj = triangle.referenced_obstacle

    if type(obj) == Receiver:
        total_dist = ray.initial_dist + intersec_w
        #return (constants.code_impulse_response_elem, ImpulseResponseElement(obj.id, int(obj.impulse_response_values_per_sec * total_dist/constants.sound_speed), ray.initial_energy))
        return (constants.code_impulse_response_elem, (obj.id, int(obj.impulse_response_values_per_sec * total_dist/constants.sound_speed), ray.initial_energy))

    if type(obj) == Wall:
        N = np.cross(triangle.U, triangle.V)
        N = normalize(N)
        refl_vec = reflect(ray.W, N)

        gen_rays = []
        gen_rays.append(Ray(ray.P + ray.W * intersec_w, refl_vec, ray.initial_energy * obj.reflection_coef, ray.initial_dist + intersec_w)) # reflected
        gen_rays.append(Ray(ray.P + ray.W * intersec_w, ray.W, ray.initial_energy * obj.refraction_coef, ray.initial_dist + intersec_w)) # refracted
        return (constants.code_ray_list, gen_rays)

    return (None, None)


sc = pyspark.SparkContext('local[*]')

env = Environment()

initial_rays = env.transmitter.generate_predefined_impulse_rays()
#initial_rays = env.transmitter.generate_symmetric_impulse_rays(2.0, 5)
#initial_rays = env.transmitter.generate_random_impulse_rays(1.0, 20) 

rays_rdd = sc.parallelize([initial_rays])
#rays_rdd = sc.parallelize(initial_rays)



# Desenha ambiente

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax_min = -6.0
ax_max = 6.0
ax.set_xlim(ax_min, ax_max)
ax.set_ylim(ax_min, ax_max)
ax.set_zlim(ax_min, ax_max)

for i in range(len(env.triangles)):
    color = 'b' if type(env.triangles[i].referenced_obstacle) == Receiver else str(random.randint(3,8)/10)
    draw_triangle(ax, env.triangles[i], color)


ax.plot3D([env.transmitter.position[0]], [env.transmitter.position[1]], [env.transmitter.position[2]], 'ko', markersize=12) # ponto na posição do transmissor



######################################################

# Computa cada raio para calcular resposta ao impulso


i = 0

#for t in env.triangles:
    #print(t.referenced_obstacle)

#abc = rays_rdd.map(test(env))
#print(abc.collect())
#exit()

#path_attenuation_coef = 0.1
#attenuation = np.exp(-path_attenuation_coef*total_dist) #attenuation with distance
#total_energy *= attenuation
impulse_elements_rdd = sc.emptyRDD()
#while i < 15:
while rays_rdd.count() > 0:
    print(f"\nBatch {i}")
    i += 1

    #if i == 9:
    #    print('a')

    rays_rdd = rays_rdd.flatMap(lambda x: x)
    #print("rays_rdd")
    #print(rays_rdd.collect())

    nearest_intersection_rdd = rays_rdd.map(lambda ray: nearest_intersection(ray, env.triangles))

    ray_list = nearest_intersection_rdd.collect()
    for ray, triangle, dist in ray_list:
        ax.plot3D([ray.P[0]], [ray.P[1]], [ray.P[2]], 'go') # ponto na posição inicial do raio
        #print(f'ray_initial_dist: {ray.initial_dist}')
        #print(f'ray_initial_energy: {ray.initial_energy}')
        if triangle is not None:
            draw_ray(ax, ray, dist, 'g')
            if type(triangle.referenced_obstacle) == Receiver:
                ax.plot3D([ray.P[0] + ray.W[0]*dist], [ray.P[1] + ray.W[1]*dist], [ray.P[2] + ray.W[2]*dist], 'ro') # ponto de interseção com receptor
        else:
            draw_ray(ax, ray, 10, 'r',  width=0.3)


    nearest_intersection_rdd = nearest_intersection_rdd.filter(func(lambda ray, triangle, w: triangle != None))
    #print("nearest_intersection_rdd filtered")
    #print(nearest_intersection_rdd.collect())      

    interactions_rdd = nearest_intersection_rdd.map(func(lambda ray, triangle, w: interact_triangle(ray, triangle, w)))
    #interactions_rdd = nearest_intersection_rdd.map(lambda x: x[1].referenced_obstacle.interact(x[0], x[1], x[2]))
    #print("interactions_rdd")
    #print(interactions_rdd.collect())

    rays_rdd = \
        interactions_rdd \
            .filter(func(lambda code, objs: code == constants.code_ray_list)) \
            .map(func(lambda code, objs: objs))
    #print("rays_rdd filtered")
    #print(rays_rdd.collect())

    impulse_elements_rdd = impulse_elements_rdd.union(
        interactions_rdd
            .filter(func(lambda code, objs: code == constants.code_impulse_response_elem))
            .map(func(lambda code, objs: objs))
    )

    rays_rdd = sc.parallelize(rays_rdd.collect())    


print("impulse_elements_rdd") 
print(impulse_elements_rdd.collect())

impulse_elements_rdd = impulse_elements_rdd.map(lambda x: ((x[0], x[1]), x[2])).reduceByKey(lambda x,y: x+y)
impulse_elements_rdd = impulse_elements_rdd.map(lambda x: (x[0][0], (x[0][1],x[1]))).groupByKey().map(lambda x: (x[0], list(x[1])))

print(impulse_elements_rdd.collect())
#r0 = b.filter(lambda x: x[0][0] == 0)
#r0.map(lambda x: (x[0][1], x[1])).collect()

#for i in impulse_elements:
#    for imp in i:
#        print(imp)

    #if i == 11:
    #    break  



plt.show()    

exit()

while not ray_queue.empty():

    i += 1
    print(f'\nray {i}')

    ray = ray_queue.get()

    ax.plot3D([ray.P[0]], [ray.P[1]], [ray.P[2]], 'go') # ponto na posição inicial do raio
    print(f'ray_initial_dist: {ray.initial_dist}')
    print(f'ray_initial_energy: {ray.initial_energy}')

    nearest_triangle, dist = nearest_intersection(ray, env.triangles)
    print(f'nearest intersection dist: {dist}')

    if nearest_triangle is not None:
        draw_ray(ax, ray, dist, 'g')

        generated_rays = nearest_triangle.referenced_obstacle.interact(ray, nearest_triangle, dist)
        print(f'{len(generated_rays)} generated rays')

        for r in generated_rays:
            if r.initial_energy >= constants.ray_min_energy:
                ray_queue.put(r)
            else:
                print(f'ignoring ray with initial_energy={r.initial_energy}')

        if len(generated_rays) == 0:
            ax.plot3D([ray.P[0] + ray.W[0]*dist], [ray.P[1] + ray.W[1]*dist], [ray.P[2] + ray.W[2]*dist], 'ro') # ponto de interseção
    else:
        print('no intersection')
        draw_ray(ax, ray, 10, 'r', width=0.3)

plt.show()


# plota resposta ao impulso
env.receiver.impulse_response = cut_impulse_response(env.receiver.impulse_response)
time_arr = np.arange(len(env.receiver.impulse_response))/env.receiver.impulse_response_values_per_sec
print(f'\nimpulse response size: {len(env.receiver.impulse_response)} ({len(env.receiver.impulse_response)/env.receiver.impulse_response_values_per_sec}s)')
plot_impulse_response(time_arr, env.receiver.impulse_response)

# salva resposta ao impulso
if len(sys.argv) > 1:    
    np.save(sys.argv[1], np.array([env.receiver.impulse_response, time_arr]))
    print(f'\nImpulse response saved to {sys.argv[1]}')
