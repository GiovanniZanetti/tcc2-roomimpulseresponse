import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def draw_ray(ray, limit, width, color):
    ax.plot3D( # line
        [ray[0][0],ray[0][0]+ray[1][0]*limit],
        [ray[0][1],ray[0][1]+ray[1][1]*limit],
        [ray[0][2],ray[0][2]+ray[1][2]*limit], 
        linewidth=width,
        color=color
    )

def normalize(V):
    return V/np.sqrt(np.sum(V**2))

def reflect(V,N):
    return normalize(V - 2*np.dot(V,N)*N)

def triangle_P_U_V_from_points(P):
    # U e V nao devem ser normalizados !!!
    U = P[1]-P[0]
    V = P[2]-P[0]
    tr = []
    tr.append(P[0])
    tr.append(U)
    tr.append(V)
    return tr


def calc_intersection(triangle, line):
    
    p0x = triangle[0][0]
    p0y = triangle[0][1]
    p0z = triangle[0][2]
    ux = triangle[1][0]
    uy = triangle[1][1]
    uz = triangle[1][2]
    vx = triangle[2][0]
    vy = triangle[2][1]
    vz = triangle[2][2]

    px = ray[0][0]
    py = ray[0][1]
    pz = ray[0][2]
    wx = ray[1][0]
    wy = ray[1][1]
    wz = ray[1][2]
    
    V = np.array([[ux, vx, -wx], [uy, vy, -wy], [uz, vz, -wz]])
    P = np.array([[px - p0x], [py - p0y], [pz - p0z]])

    
    try:
        K = np.dot(np.linalg.inv(V), P)

        print(K)

        u = K[0][0]
        v = K[1][0]
        w = K[2][0]

        if u >= 0 and v >= 0 and (u + v <= 1) and w >= 0:
            return u,v,w
        else:
            print('no intersection')
            return None
    except Exception as e:

        # matriz nao tem inversa, nao tem solucao
        print(e)
        print('no intersection')
        return None



walls = []
walls.append(triangle_P_U_V_from_points([np.array([0, 0, 0]), np.array([0, -1, 1]), np.array([0, 1, 0])])) # P0 P1 P2
walls.append(triangle_P_U_V_from_points([np.array([0.25, 1, 1]), np.array([0, -1, 1]), np.array([0, 1, 0])]))
walls.append(triangle_P_U_V_from_points([np.array([0.25, 1, 1]), np.array([0, 1, 0]), np.array([1, 1, 1])]))

ray = [np.array([2, 0.2, 0.5]), normalize(np.array([-1, 0, 0]))]  #P0 W (W deve ser normalizado para depois calcular a distancia !!!)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

colors = ['0.5','0.6', '0.7']
nearest_intersec = 1000
nearest_triangle = -1
for i in range(len(walls)):
    print(f"wall {i}:")
    wall = walls[i]
    triangles =  [(
        (wall[0][0], wall[0][1], wall[0][2]),
        (wall[0][0] + wall[1][0], wall[0][1] + wall[1][1], wall[0][2] + wall[1][2]),
        (wall[0][0] + wall[2][0], wall[0][1] + wall[2][1], wall[0][2] + wall[2][2]),
    )]
    ax.add_collection3d(Poly3DCollection(triangles, linewidths=1, color=colors[i])) #triangle

    intersec = calc_intersection(wall, ray)

    if (intersec is not None) and (intersec[2] < nearest_intersec):
        nearest_intersec = intersec[2]
        nearest_triangle = i

ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
ax.set_zlim(-1,1)

##



if nearest_triangle != -1:
    w_intersection = nearest_intersec
    print(f"nearest_triangle = {nearest_triangle}") 
    print(f"w_intersec = {w_intersection}")    

    ax.plot3D( # point of intersection
        [ray[0][0]+ray[1][0]*w_intersection],
        [ray[0][1]+ray[1][1]*w_intersection],
        [ray[0][2]+ray[1][2]*w_intersection], 
        'ro',
    )

    #calculate reflection
    N = np.cross(walls[nearest_triangle][1], walls[nearest_triangle][2])
    N = normalize(N)

    print(f"N = {N}")
    refl = reflect(ray[1], N)
    print(f"refl = {refl}")
    ray_refl = [ray[0]+ray[1]*w_intersection, refl]

    draw_ray(ray, w_intersection, 2, 'y') #ray
    draw_ray(ray_refl, 2, 1, 'y') #reflected ray

else:
    draw_ray(ray, 10, 2, 'y') #ray

plt.show()

