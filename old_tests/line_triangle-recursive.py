import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def draw_ray(ray, limit, width, color):
    ax.plot3D( # line
        [ray[0][0],ray[0][0]+ray[1][0]*limit],
        [ray[0][1],ray[0][1]+ray[1][1]*limit],
        [ray[0][2],ray[0][2]+ray[1][2]*limit], 
        linewidth=width,
        color=color
    )

def normalize(V):
    return V/np.sqrt(np.sum(V**2))

def reflect(V,N):
    return normalize(V - 2*np.dot(V,N)*N)

def triangle_P_U_V_from_points(P):
    # U e V nao devem ser normalizados !!!
    U = P[1]-P[0]
    V = P[2]-P[0]
    tr = []
    tr.append(P[0])
    tr.append(U)
    tr.append(V)
    return tr


def calc_intersection(triangle, line):
    
    p0x = triangle[0][0]
    p0y = triangle[0][1]
    p0z = triangle[0][2]
    ux = triangle[1][0]
    uy = triangle[1][1]
    uz = triangle[1][2]
    vx = triangle[2][0]
    vy = triangle[2][1]
    vz = triangle[2][2]

    px = ray[0][0]
    py = ray[0][1]
    pz = ray[0][2]
    wx = ray[1][0]
    wy = ray[1][1]
    wz = ray[1][2]
    
    V = np.array([[ux, vx, -wx], [uy, vy, -wy], [uz, vz, -wz]])
    P = np.array([[px - p0x], [py - p0y], [pz - p0z]])

    
    try:
        K = np.dot(np.linalg.inv(V), P)

        #print(K)

        u = K[0][0]
        v = K[1][0]
        w = K[2][0]

        if u >= 0 and v >= 0 and (u + v <= 1) and w > 0:
            return u,v,w
        else:
            #print('no intersection')
            return None
    except Exception as e:

        # matriz nao tem inversa, nao tem solucao
        #print(e)
        #print('no intersection')
        return None

def next_reflection(ray, walls, microphone):

    nearest_intersec = 1000
    nearest_triangle = -1

    is_mic = False

    for i in range(len(walls)):
       # if i != -1: # nao ve intersecao com a ultima parede em que refletiu
            #print(f"wall {i}:")
        wall = walls[i]

        intersec = calc_intersection(wall, ray)

        if (intersec is not None) and (intersec[2] < nearest_intersec):
            nearest_intersec = intersec[2]
            nearest_triangle = i

    for i in range(len(microphone)):
        m = microphone[i]

        intersec = calc_intersection(m, ray)

        if (intersec is not None) and (intersec[2] < nearest_intersec):
            nearest_intersec = intersec[2]
            nearest_triangle = i
            is_mic = True

    if nearest_triangle != -1:
        w_intersection = nearest_intersec
        print(f"nearest_triangle = {nearest_triangle}") 
        #print(f"w_intersec = {w_intersection}")    
        #print(f"nearest_triangle = {nearest_triangle}") 

        if not is_mic:
            #calculate reflection
            N = np.cross(walls[nearest_triangle][1], walls[nearest_triangle][2])
            N = normalize(N)

            #print(f"N = {N}")
            refl = reflect(ray[1], N)
            #print(f"refl = {refl}")
            ray_refl = [ray[0]+ray[1]*w_intersection, refl]

            return True, is_mic, ray_refl, 0.7, w_intersection
        else:
            return True, is_mic, None, 1, w_intersection
    else:
        return False, False, None, 0, 0
        #has_intersection, is_mic, ray_refl, transmission_coef, w_intersection



walls = []
walls.append(triangle_P_U_V_from_points([np.array([-1, 1, 1]), np.array([-1, 1, 0]), np.array([1, 1, 1])]))
walls.append(triangle_P_U_V_from_points([np.array([1, 1, 1]), np.array([-1, 1, 0]), np.array([1, 1, 0])]))
walls.append(triangle_P_U_V_from_points([np.array([-1, 1, 1]), np.array([-1, 1, 0]), np.array([-1, -1, 1])]))
walls.append(triangle_P_U_V_from_points([np.array([-1, -1, 0]), np.array([-1, 1, 0]), np.array([-1, -1, 1])]))
walls.append(triangle_P_U_V_from_points([np.array([-1, -1, 1]), np.array([-1, -1, 0]), np.array([1, -1, 1])]))
walls.append(triangle_P_U_V_from_points([np.array([1, -1, 0]), np.array([-1, -1, 0]), np.array([1, -1, 1])]))

mic_pos = np.array([1.8,-0.5,0.2])
microphone = []
microphone.append(triangle_P_U_V_from_points([mic_pos+np.array([-0.5, 0.5, 0.5]), mic_pos+np.array([-0.5, 0.5, 0]), mic_pos+np.array([0.5, 0.5, 0.5])]))
microphone.append(triangle_P_U_V_from_points([mic_pos+np.array([-0.5, 0.5, 0]), mic_pos+np.array([-0.5, 0.5, 0.5]), mic_pos+np.array([-0.5, -0.5, 0.5])]))

emitter_pos = np.array([1, 0.8, 0.5])
initial_rays = [] #P0 W (W deve ser normalizado para depois calcular a distancia !!!)
initial_rays.append([emitter_pos, normalize(np.array([-1, 0.7, 0]))])
initial_rays.append([emitter_pos, normalize(np.array([-0.2, -0.2, 0]))])
initial_rays.append([emitter_pos, normalize(np.array([-1, -0.1, 0]))])
initial_rays.append([emitter_pos, normalize(np.array([-1, -0.2, 0]))])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')



#draw walls
colors = ['0.5','0.5', '0.7','0.7', '0.6','0.6']
for i in range(len(walls)):
    wall = walls[i]
    triangles =  [(
        (wall[0][0], wall[0][1], wall[0][2]),
        (wall[0][0] + wall[1][0], wall[0][1] + wall[1][1], wall[0][2] + wall[1][2]),
        (wall[0][0] + wall[2][0], wall[0][1] + wall[2][1], wall[0][2] + wall[2][2]),
    )]
    ax.add_collection3d(Poly3DCollection(triangles, linewidths=1, color=colors[i])) #triangle

#draw microphone
for i in range(len(microphone)):
    m = microphone[i]
    triangles =  [(
        (m[0][0], m[0][1], m[0][2]),
        (m[0][0] + m[1][0], m[0][1] + m[1][1], m[0][2] + m[1][2]),
        (m[0][0] + m[2][0], m[0][1] + m[2][1], m[0][2] + m[2][2]),
    )]
    ax.add_collection3d(Poly3DCollection(triangles, linewidths=1, color='b'))
##

max_iterations = 10
impulse_response = np.zeros(1000)

for initial_ray in initial_rays:

    ax.plot3D( 
        [initial_ray[0][0]],
        [initial_ray[0][1]],
        [initial_ray[0][2]], 
        'go',
    )

    ray = initial_ray
    total_dist = 0
    total_energy = 2
    path_attenuation_coef = 0.1

    for i in range(max_iterations):

        print(f'step {i}')

        intersected, is_mic, ray_refl, transmission_coef, dist = next_reflection(ray, walls, microphone)

        if not intersected:
            draw_ray(ray, 10, total_energy, 'r') #ray
            print('no intersection')
            break
        
        draw_ray(ray, dist, total_energy, 'g') #ray

        total_dist = total_dist + dist
        total_energy = total_energy * transmission_coef

        print(f'total_dist: {total_dist}')
        print(f'total_energy: {total_energy}')

        if not is_mic:
            ray = ray_refl
        else:            
            attenuation = np.exp(-path_attenuation_coef*total_dist) #attenuation with distance
            print(f"path attenuation = {attenuation}")
            total_energy *= attenuation
            impulse_response[int(total_dist*100)] += total_energy
            ax.plot3D( # point of intersection
                [ray[0][0] + ray[1][0]*dist],
                [ray[0][1] + ray[1][1]*dist],
                [ray[0][2] + ray[1][2]*dist], 
                'ro',
            )
            print('intersection with microphone')
            break

#print(impulse_response)

ax.set_xlim(-1.2,1.2)
ax.set_ylim(-1.2,1.2)
ax.set_zlim(-1.2,1.2)

plt.show()

plt.plot(range(len(impulse_response)), impulse_response)
plt.show()

