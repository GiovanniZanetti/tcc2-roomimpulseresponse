import numpy as np
import matplotlib.pyplot as plt

def normalize(v):
    return v/np.sqrt(np.sum(v**2))

def reflect(normal, direction):
    direction = -direction
    return 2*(np.dot(normal,direction))*normal-direction

def normal_from_parametric(l):
    v = normalize(np.array([l[0],l[2]]))
    return np.array([v[1],-v[0]])


def from_angle_point(degrees, x, y):
    rad = np.pi*degrees/180
    a = np.cos(rad)
    c = np.sin(rad)
    b = x
    d = y
    return np.array([a,b,c,d])
    

def intersec(l1,l2):
    a1 = l1[0]
    b1 = l1[1]
    c1 = l1[2]
    d1 = l1[3]
    a2 = l2[0]
    b2 = l2[1]
    c2 = l2[2]
    d2 = l2[3]
    X = np.array([[a1, -a2], [c1, -c2]])
    Y = np.array([[b2-b1], [d2-d1]])
    T = np.dot(np.linalg.inv(X), Y)
    return T

#equacao parametrica da reta:
#x = a*t+b
#y = c*t+d

#intersecao entre duas retas:
#a1*t1 + b1 = a2*t2 + b2
#c1*t1 + d1 = c2*t2 + d2
#resolvendo o sistema:
# --->  t2 = (a1*(d2-d1) - c1*(b2-b1))/(c1*a2 - c2*a1)
#ou por matriz:
#|a1 -a2|*|t1|=|b2-b1|
#|c1 -c2| |t2| |d2-d1|
#X*T=Y
# ---> T=X⁻1*Y


# cria raio especificado com angulo, x e y
ray = from_angle_point(110,25,15)
print(f'ray: {ray}')

walls = []
walls.append(np.array([1,-0.1,-1,-50]))
walls.append(np.array([-1,-40,4.5,5.5]))
walls.append(np.array([1,-0.1,0.1,100]))
walls.append(np.array([0,90,5,-5.5]))

near_intersec = 10000
near_wall_idx = 0
for i in range(len(walls)):
    wall = walls[i]
    t_intersec = intersec(ray,wall)[0]
    print(t_intersec)

	# procura intersecao mais perto e na frente do raio
    if t_intersec >= 0 and t_intersec < near_intersec:
        near_intersec = t_intersec
        near_wall_idx = i

    plt.plot([wall[0]*-100+wall[1], wall[0]*100+wall[1]], [wall[2]*-100+wall[3], wall[2]*100+wall[3]], color='b', linestyle='-', linewidth=1)
    #xp = ray[0]*t_intersec+ray[1]
    #yp = ray[2]*t_intersec+ray[3]
    #plt.plot([xp], [yp], 'ro')

#plota
print(f"near: {near_intersec}")
xp = ray[0]*near_intersec+ray[1]
yp = ray[2]*near_intersec+ray[3]
plt.plot([xp], [yp], 'go')

plt.plot([ray[1]], [ray[3]], 'bo') #ponto inicial do raio
plt.plot([ray[0]*0+ray[1], ray[0]*near_intersec+ray[1]], [ray[2]*0+ray[3], ray[2]*near_intersec+ray[3]], color='k', linestyle='-', linewidth=1)

# calcula raio refletido
near_wall_normal = normal_from_parametric(walls[near_wall_idx])
ray_direction = np.array([ray[0], ray[2]])
reflected_direction = reflect(near_wall_normal, ray_direction)
reflected_ray = np.array([reflected_direction[0],ray[0]*near_intersec+ray[1],reflected_direction[1],ray[2]*near_intersec+ray[3]])
plt.plot([reflected_ray[0]*0+reflected_ray[1], reflected_ray[0]*100+reflected_ray[1]], [reflected_ray[2]*0+reflected_ray[3], reflected_ray[2]*100+reflected_ray[3]], color='y', linestyle='-', linewidth=1)

axes = plt.gca()
axes.set_xlim([-150,150])
axes.set_ylim([-150,150])
plt.show()

