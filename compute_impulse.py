from queue import Queue
from objects import ImpulseResponse
from functions import inverse_fft, interact_triangle, nearest_intersection, get_timestamp_ms
import constants


def compute_impulse_response(initial_rays, env, rays_to_draw):

    # computa resposta ao impulso no modo normal

    ray_queue = Queue()
    for r in initial_rays:
        ray_queue.put(r)

    impulse_elements = []

    print('Computing impulse response on normal mode ...')
    start_timestamp = get_timestamp_ms()

    while not ray_queue.empty():

        ray = ray_queue.get()
        #print(ray)

        nearest_triangle, dist = nearest_intersection(ray, env.triangles)

        rays_to_draw.append((ray, dist))

        if nearest_triangle is not None:        

            interactions = interact_triangle(ray, nearest_triangle, dist, env.sound_speed, env.sound_attenuation_freq_coef)

            for interaction in interactions:
                if interaction[0] == constants.code_ray:
                    ray_queue.put(interaction[1])
                elif interaction[0] == constants.code_impulse_elem:
                    impulse_elements.append(interaction[1])

    #print(impulse_elements)

    print(f'### Processing time: {get_timestamp_ms() - start_timestamp}ms ###')

    # Calcula respostas ao impulso

    start_timestamp = get_timestamp_ms()

    for receiver in env.receivers:

        elems = list(map(lambda x: (x[1], x[2]), filter(lambda x: x[0] == receiver.id, impulse_elements)))
        elems = list(filter(lambda x: x[1].mean() >= constants.ray_min_amplitude_impulse_response, elems))
        elems = list(map(lambda x: (x[0], inverse_fft(x[1], fir_num_coefs=constants.fir_num_coefs, window=None)), elems))

        #for e in elems:
        #    print(e[0])
        
        if len(elems) > 0:
            receiver.impulse_response = ImpulseResponse.load_from_elements(elems, constants.impulse_response_sample_rate)


    print(f'### Processing time - impulse response : {get_timestamp_ms() - start_timestamp}ms ###')   