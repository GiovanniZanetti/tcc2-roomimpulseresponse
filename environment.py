import numpy as np
import json
import random

import functions
import objects
import constants
import wavefront_obj


class Wall:

    def __init__(self, reflection_filter, refraction_filter):
        self.reflection_filter = reflection_filter
        self.refraction_filter = refraction_filter

class Receiver:

    def __init__(self, id, name=None):
        self.id = id
        self.name = name
        self.impulse_response = None


class Transmitter:

    def __init__(self, position, direction, aperture_degrees):

        self.position = position
        self.direction = functions.normalize(direction)
        self.aperture_radians = np.pi*aperture_degrees/180

    def generate_single_impulse_ray(self, amplitude=1.0):

        # gera um raio unico na direção do transmissor

        #return [objects.Ray(self.position, self.direction, energy)]      
        return [objects.Ray(self.position, self.direction, initial_amplitude=amplitude)]      

    def generate_symmetric_impulse_rays(self, amplitude, num_rays):

        # gera raios com base na direção do transmissor e do angulo de abertura

        rays = []

        theta1 = np.arctan2(np.sqrt(self.direction[0]**2 + self.direction[1]**2), self.direction[2])
        theta2 = np.arctan2(self.direction[1], self.direction[0])
        for t1 in np.linspace(theta1-self.aperture_radians/2, theta1+self.aperture_radians/2, num_rays):
            for t2 in np.linspace(theta2-self.aperture_radians/2, theta2+self.aperture_radians/2, num_rays):
                rays.append(objects.Ray(
                    self.position, 
                    np.array([np.sin(t1)*np.cos(t2), np.sin(t1)*np.sin(t2), np.cos(t1)]), 
                    initial_amplitude=amplitude
                ))
        return rays

    def generate_random_impulse_rays(self, amplitude, num_rays):

        # gera raios com base na direção do transmissor e do angulo de abertura, aleatoriamente

        rays = []

        theta1 = np.arctan2(np.sqrt(self.direction[0]**2 + self.direction[1]**2), self.direction[2])
        theta2 = np.arctan2(self.direction[1], self.direction[0])
        for i in range(num_rays):
            t1 = random.uniform(theta1-self.aperture_radians/2, theta1+self.aperture_radians/2)
            t2 = random.uniform(theta2-self.aperture_radians/2, theta2+self.aperture_radians/2)
            rays.append(objects.Ray(
                self.position, 
                np.array([np.sin(t1)*np.cos(t2), np.sin(t1)*np.sin(t2), np.cos(t1)]), 
                initial_amplitude=amplitude/num_rays
            ))
        return rays


class Environment:
    
    def __init__(self, triangles=[], receivers=[], transmitters=[], sound_speed=0, sound_attenuation_freq_coef=[]):
        self.triangles = triangles
        self.receivers = receivers
        self.transmitters = transmitters
        self.sound_speed = sound_speed  # m/s
        self.sound_attenuation_freq_coef = sound_attenuation_freq_coef  # coeficiente de atenuação para cada frequencia

    @staticmethod
    def load_from_file(obj_filename, json_filename):

        # carrega ambiente com base nas configurações do arquivo json 
        # e da geometria do arquivo obj

        triangles = []
        receivers = []
        transmitters = []

        print(f'Loading environment params from {json_filename}')
        with open(json_filename, 'r') as f:
            env_params = json.loads(f.read())
            
        print(f'Loading environment geometry from {obj_filename}')
        objects_geometry = wavefront_obj.load_geometry_from_obj(obj_filename)

        for obj_name in objects_geometry:

            obj_type = obj_name.split('_')[0]
            obj_triangles = objects_geometry[obj_name]

            print(f'obj_name: {obj_name}')
            print(f'obj_type: {obj_type}')
            #print(obj_triangles)

            referenced_obj = None
            add_triangles = False

            param_obj_name = obj_name
            if obj_name not in env_params['object_params']:
                param_obj_name = "defaults"

            if obj_type == 'Wall':   

                refl_filter = env_params['object_params'][param_obj_name]['reflection_filter']
                refr_filter = env_params['object_params'][param_obj_name]['refraction_filter']
                print(f'refl_filter: {refl_filter}')
                print(f'refr_filter: {refr_filter}')
                referenced_obj = Wall(functions.build_filter_arr(refl_filter), functions.build_filter_arr(refr_filter))

                add_triangles = True

            elif obj_type == 'Receiver':

                referenced_obj = Receiver(len(receivers), obj_name)
                receivers.append(referenced_obj)
                add_triangles = True

            elif obj_type == 'Transmitter':

                aperture_degrees = env_params['object_params'][param_obj_name]['aperture_degrees']
                t = obj_triangles[0]
                direction = functions.calculate_normal(np.array(t[0]), np.array(t[1]), np.array(t[2]))
                position = (np.array(t[0]) + np.array(t[1]) + np.array(t[2])) / 3

                print(f'aperture_degrees: {aperture_degrees}, direction: {direction}')
                transmitters.append(Transmitter(position, direction, aperture_degrees))

            if add_triangles:
                for t in obj_triangles:                    
                    triangles.append(objects.Triangle(np.array(t[0]), np.array(t[1]), np.array(t[2]), referenced_obj))

        print(f'{len(triangles)} triangles loaded')

        print(f"sound_speed: {env_params['sound_speed']} m/s")
        print(f"sound_attenuation_freq_coef: {env_params['sound_attenuation_freq_coef']}")

        return Environment(
            triangles, 
            receivers, 
            transmitters, 
            env_params['sound_speed'],
            functions.build_filter_arr(env_params['sound_attenuation_freq_coef'])
        )
  