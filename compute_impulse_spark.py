from queue import Queue
from objects import ImpulseResponse
from functions import inverse_fft, interact_triangle, nearest_intersection, get_timestamp_ms
import constants
import pyspark


def process_ray(ray, broadcast_env):

    ray_queue = Queue()
    ray_queue.put(ray)
    impulse_elements = []
    i = 0

    while not ray_queue.empty():

        ray = ray_queue.get()
        #print(ray)

        nearest_triangle, dist = nearest_intersection(ray, broadcast_env.value.triangles)

        if nearest_triangle is not None:        

            interactions = interact_triangle(ray, nearest_triangle, dist, broadcast_env.value.sound_speed, broadcast_env.value.sound_attenuation_freq_coef)

            for interaction in interactions:
                if interaction[0] == constants.code_ray:
                    ray_queue.put(interaction[1])
                elif interaction[0] == constants.code_impulse_elem:
                    impulse_elements.append(interaction[1])

    return impulse_elements
        

def compute_impulse_response_spark(initial_rays, env, num_workers):

    # computa resposta ao impulso no modo spark

    sc = pyspark.SparkContext(f'local[{num_workers}]')

    print(sc.getConf().getAll())

    broadcast_env = sc.broadcast(env)
    rays_rdd = sc.parallelize(initial_rays, numSlices=12)
    print(f'rdd num partitions: {rays_rdd.getNumPartitions()}')

    print('Computing impulse response on spark mode ...')
    start_timestamp = get_timestamp_ms()

    impulse_elements_rdd = rays_rdd.map(lambda ray: process_ray(ray, broadcast_env))
    impulse_elements_rdd = impulse_elements_rdd.flatMap(lambda x: x)
    impulse_elements_rdd = impulse_elements_rdd.filter(lambda x: x[2].mean() >= constants.ray_min_amplitude_impulse_response)
    impulse_elements_rdd = impulse_elements_rdd.map(lambda x: (x[0], x[1], inverse_fft(x[2], window=None)))
    impulse_elements_rdd = impulse_elements_rdd.map(lambda x: (x[0], (x[1], x[2])))
    impulse_elements_rdd = impulse_elements_rdd.groupByKey().mapValues(list)
    impulse_elements_rdd = impulse_elements_rdd.map(lambda x: (x[0], ImpulseResponse.load_from_elements(x[1], constants.impulse_response_sample_rate)))

    #impulse_elements_rdd = impulse_elements_rdd \
    #    .filter(lambda x: x[2].mean() >= constants.ray_min_amplitude_impulse_response) \
    #    .map(lambda x: (x[0], x[1], inverse_fft(x[2], window=None))) \
    #    .map(lambda x: (x[0], (x[1], x[2]))) \
    #    .groupByKey().mapValues(list) \
    #    .map(lambda x: (x[0], ImpulseResponse.load_from_elements(x[1], constants.impulse_response_sample_rate)))

    impulse_responses = impulse_elements_rdd.collect()

    print(f'### Processing time: {get_timestamp_ms() - start_timestamp}ms ###')

    print(impulse_responses)

    for i in impulse_responses:
        env.receivers[i[0]].impulse_response = i[1]

    print(f'### Total processing time: {get_timestamp_ms() - start_timestamp}ms ###')