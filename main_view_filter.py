import matplotlib.pyplot as plt
import numpy as np
import sys
import json

import functions
import constants

env_file = sys.argv[1]
print(f'env_file: {env_file}')

with open(env_file, 'r') as f:
    params = json.loads(f.read())

filter_names = sys.argv[2:]
for filter_name in filter_names:
    filter_name = filter_name.split('/')
    print(f'filter_name: {filter_name}')

    fi = params
    for a in filter_name:
        fi = fi[a]
    print(fi)

    fi_arr = functions.build_filter_arr(fi)
    x_arr = np.linspace(0, constants.impulse_response_sample_rate/2, len(fi_arr))
    plt.plot(x_arr, fi_arr)

plt.xlim(0.0, constants.impulse_response_sample_rate/2)
plt.ylim(0.0, 1.1)
#plt.legend(filter_names, prop={'size': 8})
#plt.title(f"Resposta do filtro")
plt.xlabel("Frequência (Hz)")
plt.ylabel("Amplitude")
plt.show()