import matplotlib.pyplot as plt
import numpy as np
import sys
import scipy.io.wavfile
from objects import ImpulseResponse
from plot_utils import plot_impulse_response

def load_wav(filename, normalize=False):

    # carrega arquivo wave

    sample_rate, input_data = scipy.io.wavfile.read(filename)
    print(f'Loaded {filename}: sample rate: {sample_rate}, data type: {input_data.dtype}, data size: {len(input_data)}')
    if normalize:
        if str(input_data.dtype).startswith('int'):
            print(f'input normalized from [{np.iinfo(input_data.dtype).min}, {np.iinfo(input_data.dtype).max}] to [-1.0, +1.0]')
            input_data = input_data / np.iinfo(input_data.dtype).max
        elif str(input_data.dtype).startswith('float'):            
            print(f'input normalized from [{np.finfo(input_data.dtype).min}, {np.finfo(input_data.dtype).max}] to [-1.0, +1.0]')
            input_data = input_data / np.finfo(input_data.dtype).max

    return sample_rate, input_data

def save_wav(filename, sample_rate, data):

    # salva arquivo wav

    scipy.io.wavfile.write(filename, sample_rate, data)
    print(f'Saved {filename}: sample rate: {sample_rate}, data type: {data.dtype}, data size: {len(data)}')


input_file = sys.argv[1]
impulse_response_file = sys.argv[2]

output_file = "outputs/generated_sound.wav"

print(f'input_file: {input_file}')
print(f'impulse_response_file: {impulse_response_file}')
print(f'output_file: {output_file}')

# abre o arquivo de entrada

sample_rate, input_data = load_wav(input_file)

# abre o arquivo com a resposta ao impulso

impulse_response = ImpulseResponse.load_from_npy_file(impulse_response_file)
print(f'impulse response size: {impulse_response.size()} ({impulse_response.size_in_seconds()}s)')
plot_impulse_response(impulse_response.energy_arr, impulse_response.time_arr)

#impulse_response_sampl = impulse_response.get_sampled_array(sample_rate)
impulse_response_sampl = impulse_response.get_interp_resampled_array(sample_rate)
print(f'impulse response size sampled: {len(impulse_response_sampl)}')
plt.plot(impulse_response_sampl)
plt.show()


# faz a convolução e salva o arquivo de saida

output_data = np.convolve(impulse_response_sampl, input_data)
output_data = output_data.astype(input_data.dtype)
save_wav(output_file, sample_rate, output_data)

