import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import random
import numpy as np
from environment import Receiver, Environment


def draw_ray(ax, ray, limit, color, width=1):

    # desenha um raio

    ax.plot3D( # line
        [ray.P[0],ray.P[0]+ray.W[0]*limit],
        [ray.P[1],ray.P[1]+ray.W[1]*limit],
        [ray.P[2],ray.P[2]+ray.W[2]*limit], 
        linewidth=width,
        color=color
    )

def draw_triangle(ax, triangle, color, linewidths=1, edgecolor=None):

    # desenha um triangulo

    if edgecolor is None:
        edgecolor = color

    t = [(
        (triangle.P[0], triangle.P[1], triangle.P[2]),
        (triangle.P[0] + triangle.U[0], triangle.P[1] + triangle.U[1], triangle.P[2] + triangle.U[2]),
        (triangle.P[0] + triangle.V[0], triangle.P[1] + triangle.V[1], triangle.P[2] + triangle.V[2]),
    )]
    ax.add_collection3d(Poly3DCollection(t, linewidths=linewidths, color=color, edgecolor=edgecolor))

def draw_environment(ax, ax_size, env, rays_to_draw=[], wireframe=False):

    # desenha ambiente

    ax.set_xlim(-ax_size, ax_size)
    ax.set_ylim(-ax_size, ax_size)
    ax.set_zlim(-ax_size, ax_size)

    for i in range(len(env.triangles)):
        if not wireframe:
            color = 'b' if type(env.triangles[i].referenced_object) == Receiver else str(random.randint(3,8)/10)
            draw_triangle(ax, env.triangles[i], color)
        else:
            color = 'b' if type(env.triangles[i].referenced_object) == Receiver else 'k'
            draw_triangle(ax, env.triangles[i], (0,0,0,0), linewidths=0.5, edgecolor=color)


    for transmitter in env.transmitters:
        ax.plot3D([transmitter.position[0]], [transmitter.position[1]], [transmitter.position[2]], 'ko', markersize=12) # ponto na posição do transmissor

    for item in rays_to_draw:
        ray = item[0]
        dist = item[1]
        if dist != np.inf:
            draw_ray(ax, ray, dist, 'g')
            ax.plot3D([ray.P[0] + ray.W[0]*dist], [ray.P[1] + ray.W[1]*dist], [ray.P[2] + ray.W[2]*dist], 'ro') # ponto de interseção
        else:
            draw_ray(ax, ray, 10, 'r', width=0.3)

def plot_impulse_response(energy_arr, time_arr, receiver_name=None, xlim=None, ylim=None, stem=False, file_to_save=None, show=True, color=None, alpha=None):

    # plota resposta ao impulso

    if receiver_name is not None:
        plt.title(f"Resposta ao impulso - {receiver_name}")
    else:
        plt.title(f"Resposta ao impulso")     
    
    if stem:
        plt.stem(time_arr, energy_arr, markerfmt=" ")
    else:
        plt.plot(time_arr, energy_arr, color=color, alpha=alpha)

    #plt.axvline(0, linestyle="dashed", linewidth=1, color='k')

    if xlim is not None:
        plt.xlim(xlim[0], xlim[1])
    if ylim is not None:
        plt.ylim(ylim[0], ylim[1])

    plt.xlabel("Tempo (s)")
    plt.ylabel("Amplitude")

    if file_to_save is not None:
        plt.savefig(file_to_save)

    if show:
        plt.show()