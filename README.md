
TCC - GIOVANNI LUIZ ZANETTI

ENGENHARIA DE COMPUTAÇÂO - UTFPR


SIMULAÇÃO DA RESPOSTA ACÚSTICA AO IMPULSO EM AMBIENTES TRIDIMENSIONAIS UTILIZANDO COMPUTAÇÃO PARALELA

Execução do programa:

```
python main_compute_impulse.py [--drawenv] [--plotresponse] [--spark] [--numworkers NUMWORKERS] [--numrays NUMRAYS] env envparams
```

* env: geometria do ambiente (arquivo .obj)
* envparams: parâmetros do ambiente (arquivo .json)
* --drawenv: desenha o ambiente
* --plotresponse: plota as respostas ao impulso geradas
* --spark: executa no modo spark
* --numworkers: número de workers usados pelo spark
* --numrays: número de raios a serem emitidos por cada emissor

Formato arquivo de parâmetros (.json):

```
{
    "sound_speed": 330,
    "sound_attenuation_freq_coef": [
        [0, 0.0]
    ],
    "object_params": {
        "defaults": {
            "reflection_filter": [
                [400, 0.9],
                [401, 0.0]
            ],
            "refraction_filter": [
                [0, 0.0]
            ],			
            "aperture_degrees": 180
        }
    }
}
```

O arquivo contém os parâmetros do ambiente, dos seus objetos, e dos filtros das paredes.

Todos os parâmetros que descrevem filtros são especificados em formato de lista, onde são descritas as frequências e seus <br> respectivos valores, onde os valores intermediários entre as frequências informadas são interpolados linearmente. <br> Se apenas uma frequência é especificada, o mesmo valor é aplicado é para todas as frequências.

O campo *object_params* possui os parâmetros dos objetos do ambiente, cada subitem desse grupo pode ser um objeto <br> específico pelo seu nome, ou o subitem *defaults*, que contém parâmetros padrão que são aplicados a todos os objetos <br> que não tiveram seus parâmetros definidos nesse arquivo.

* **sound_speed** especifica a velocidade do som no ambiente em *m/s*.
* **sound_attenuation_freq_coef** é um parâmetro no mesmo formato do filtro, que especifica o coeficiente de atenuação do som no ar para cada frequência.
* **reflection_filter** especifica o filtro com os coeficientes de reflexão de determinada parede
* **refraction_filter** especifica o filtro com os coeficientes de refração de determinada parede
* **aperture_degrees** especifica o ângulo de abertura em graus de um emissor em específico


