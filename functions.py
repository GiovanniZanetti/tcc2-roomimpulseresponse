import numpy as np
from scipy.signal import firls, freqz
import time

import constants
import objects
import environment

import matplotlib.pyplot as plt


def get_timestamp_ms():

    # retorna timestamp atual em milisegundos

    return int(time.time()*1000)

def normalize(V):

    # normaliza o vetor V

    return V/np.sqrt(np.sum(V**2))

def reflect(V, N):

    # reflete o vetor V com base na normal N

    return normalize(V - 2*np.dot(V,N)*N)

def calculate_normal(P0, P1, P2):

    # calcula o vetor normal do triangulo formado pelos pontos P0, P1 e P2

    return normalize(np.cross(P1 - P0, P2 - P0))

def inverse_fft(frequency_arr, fir_num_coefs=0, window=None):

    if fir_num_coefs == 0:
        tmp = np.fft.irfft(frequency_arr)
        time_arr = np.zeros(len(tmp))
        time_arr[:len(tmp)//2] = tmp[len(tmp)//2:]
        time_arr[len(tmp)//2:] = tmp[:len(tmp)//2]        
    else:
        freq_arr_d2 = np.diff(np.diff(frequency_arr))
        points = np.argwhere(np.abs(freq_arr_d2) > 0).reshape(1, -1)[0] + 1
        values = frequency_arr[points]
        points = points/float(len(frequency_arr))
        points = np.r_[0, points, 1]
        values = np.r_[frequency_arr[0], values, frequency_arr[-1]]
        fir = firls(fir_num_coefs, points, values)
        w, F = freqz(fir, fs=constants.impulse_response_sample_rate)
        time_arr = np.fft.irfft(F)

        #plt.plot(frequency_arr)
        #plt.show()

        #plt.plot(w, np.abs(F), color='k')
        #plt.ylim((0,1.1))
        #plt.xlim((0,constants.impulse_response_sample_rate/2))
        #plt.xlabel("Frequência (Hz)")
        #plt.ylabel("Amplitude")
        #plt.show()

        #plt.plot(time_arr)
        #plt.show()
    
    if window is not None:
        time_arr = time_arr * window
    return time_arr  

def nearest_intersection(ray, triangles):

    # procura a interseção mais proxima, entre o raio e os triangulos na lista

    min_dist = np.inf
    nearest_triangle = None

    for triangle in triangles:
        intersec = triangle.calc_intersection(ray)
        if intersec is not None: 
            intersec_w = intersec[2]
            if (intersec_w < min_dist) and (intersec_w > constants.min_intersection_dist): # evita raio colidir na parede em que foi gerado
                min_dist = intersec_w
                nearest_triangle = triangle

    return nearest_triangle, min_dist

def append_if_min_energy(l, ray):

    # adiciona raio à lista apenas se a energia é >= constants.ray_min_energy
    # caso contrario o raio é ignorado

    if ray.mean_amplitude() >= constants.ray_min_amplitude:
        l.append((constants.code_ray, ray))

def interact_triangle(ray, triangle, intersec_w, sound_speed, sound_attenuation_freq_coef):

    # processa interação do raio com o triangulo, dependendo do tipo do objeto em que colidiu

    obj = triangle.referenced_object

    ray_final_dist = ray.initial_dist + intersec_w
    ray_final_point = ray.P + ray.W * intersec_w
    ray_final_amplitude = ray.frequency_amplitude * np.exp(-sound_attenuation_freq_coef * intersec_w)  # atenuação "no ar"

    return_list = []

    # interseção com o receptor

    if type(obj) == environment.Receiver:

        # parcela de energia gerada na reposta ao impulso do receptor
        return_list.append(
            (constants.code_impulse_elem, 
                (
                    obj.id, 
                    int(constants.impulse_response_sample_rate * ray_final_dist/sound_speed), 
                    ray_final_amplitude
                )
            )
        )

        # raio que "atravessa" e continua depois do receptor
        append_if_min_energy(
            return_list,
            objects.Ray(
                ray_final_point,
                ray.W,
                frequency_amplitude=ray_final_amplitude,
                initial_dist=ray_final_dist
            )
        )        

    # interseção com a parede

    elif type(obj) == environment.Wall:

        #N = np.cross(triangle.U, triangle.V)
        #N = normalize(N)
        N = triangle.N
        refl_vec = reflect(ray.W, N)

        # raio refletido pela parede
        append_if_min_energy(
            return_list,
            objects.Ray(
                ray_final_point,
                refl_vec,
                frequency_amplitude=ray_final_amplitude * obj.reflection_filter,
                initial_dist=ray_final_dist
            )
        ) 

        # raio refratado na parede
        append_if_min_energy(
            return_list,
            objects.Ray(
                ray_final_point,
                ray.W,
                frequency_amplitude=ray_final_amplitude * obj.refraction_filter,
                initial_dist=ray_final_dist
            )
        )

    return return_list

def get_freq_idx(freq):

    # calcula posição no array do filtro com base na frequencia, tamanho do filtro e sample rate
    return round(constants.fft_size * freq / (constants.impulse_response_sample_rate / 2))

def build_filter_arr(params):

    # constroi array do filtro com base nos parametros passados pelo dicionario

    fx = np.array(params)[:,0]
    fy = np.array(params)[:,1]

    fx = list(map(lambda freq: get_freq_idx(freq), fx))

    fx_resampled = np.arange(0, constants.fft_size)
    fy_resampled = np.interp(fx_resampled, fx, fy)

    return fy_resampled
