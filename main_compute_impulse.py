########

## O NumPy usa por baixo funções algébricas de uma implementação do BLAS (Basic Linear Algebra Subprograms), 
## seja o OpenBLAS, MKL ou Lapack, que por padrão já suporta paralelismo em algumas determinadas funções,
## o que aparentava estar ocasionando um mal funcionamento dentro do Spark 
## Para isso a variável de ambiente abaixo seta para 1 o numero máximo de threads para cada processo, evitando
## a paralelização automática da biblioteca BLAS, assim evitando problemas com o Spark e resultando em tempos de 
## processamento mais coerentes

import os
os.environ["OMP_NUM_THREADS"] = "1"

########

import matplotlib.pyplot as plt

from environment import Receiver, Environment
from plot_utils import plot_impulse_response, draw_environment
from arg_utils import get_args_compute_impulse

from compute_impulse import compute_impulse_response
from compute_impulse_spark import compute_impulse_response_spark

from os import getpid

print(f'PID: {getpid()}')

args = get_args_compute_impulse()
print(f"args: {args}")

env = Environment.load_from_file(args['env_geometry_file'], args['env_params_file'])


# Coloca na fila raios gerados pelo transmissor

initial_rays = []
for transmitter in env.transmitters:
    #rays = transmitter.generate_single_impulse_ray()
    #rays = transmitter.generate_symmetric_impulse_rays(1.0, 10)
    #rays = transmitter.generate_random_impulse_rays(10.0, 1000)
    rays = transmitter.generate_random_impulse_rays(1.0, args['num_rays'])
    for r in rays:
        initial_rays.append(r)


rays_to_draw = []

# Computa resposta ao impulso

if args['spark_mode'] == True:
    compute_impulse_response_spark(initial_rays, env, args['num_workers'])  # modo spark
else:
    compute_impulse_response(initial_rays, env, rays_to_draw)  # modo normal


# Desenha ambiente

if args['draw_env'] == True:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    draw_environment(ax, 6.0, env, rays_to_draw, wireframe=True)
    plt.show()      


# Plota e salva respostas ao impulso

for receiver in env.receivers:
    if receiver.impulse_response is not None:

        if args['plot_response'] == True:
            plot_impulse_response(receiver.impulse_response.energy_arr, receiver.impulse_response.time_arr, receiver.name if receiver.name is not None else f'Receiver {receiver.id}', file_to_save=f'outputs/generated_h{receiver.id}.png')
        
        print(f'receiver {receiver.id}: impulse response size: {receiver.impulse_response.size()} ({receiver.impulse_response.size_in_seconds()}s)')
        #receiver.impulse_response.save_to_npy_file(f'outputs/generated_impulse-receiver{receiver.id}.npy')
        receiver.impulse_response.save_to_npy_file(f'outputs/generated_h{receiver.id}.npy')

    else:
        print(f'receiver {receiver.id}: no impulse response')
  
