## Constantes globais

# ignora raios com energia menor que essa variavel
ray_min_amplitude = 0.0000001

# ignora raios com energia menor que essa variavel na resposta ao impulso
ray_min_amplitude_impulse_response = 0.000001

# distancia minima para ser detectada uma interseção, evita raio colidir na mesma parede em que foi gerado
min_intersection_dist = 0.001

# taxa de amostragem da resposta ao impulso
impulse_response_sample_rate = 22050 

# tamanho do array de energia do raio por frequencia 
# (corresponde a metade do tamanho, pois para o caso em que o sinal no tempo é puramente real, a outra metade é espelhada)
fft_size = 512

# numero de coeficientes do filtro fir a ser gerado
# se for igual a zero, nao é gerado filtro fir
fir_num_coefs = 151


code_ray = "ray"
code_impulse_elem = "impulse_elem"