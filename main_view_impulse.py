import matplotlib.pyplot as plt
import numpy as np
import sys
from objects import ImpulseResponse
from plot_utils import plot_impulse_response

def mean_squared_error(impulse1, impulse2, time_limits=(None, None), decay=False):

    # calcula o erro medio quadrático

    min_t = max(impulse1.time_arr[0], impulse2.time_arr[0])
    if time_limits[0] is not None:
        min_t = max(min_t, time_limits[0])

    max_t = min(impulse1.time_arr[-1], impulse2.time_arr[-1])
    if time_limits[1] is not None:
        max_t = min(max_t, time_limits[1])        

    print(f'min_t: {min_t}s, max_t: {max_t}s')

    if decay:
        e1 = impulse1.decay_arr[impulse1.get_time_index(min_t) : impulse1.get_time_index(max_t)]
        e2 = impulse2.decay_arr[impulse2.get_time_index(min_t) : impulse2.get_time_index(max_t)]        
    else:
        e1 = impulse1.energy_arr[impulse1.get_time_index(min_t) : impulse1.get_time_index(max_t)]
        e2 = impulse2.energy_arr[impulse2.get_time_index(min_t) : impulse2.get_time_index(max_t)]
    
    #e1 = np.array([0,1,0,0,0,1,0])
    #e2 = np.array([0,0,0, 1,1,1, 0,0,0, 0,0,0, 0,0,0, 1,1,1, 0,0,0])
    #e2 = np.ones(len(e2)) * np.mean(e1)
    #e2 = np.zeros(len(impulse2.energy_arr))

    if len(e1) > len(e2):
        e_M = e1
        e_m = e2
    else:
        e_M = e2
        e_m = e1

    print(f'e_M: size: {len(e_M)} std: {np.std(e_M)} mean: {np.mean(e_M)}')
    print(f'e_m: size: {len(e_m)} std: {np.std(e_m)} mean: {np.mean(e_m)}')

    err = 0
    for i in range(len(e_M)):
        idx_m = int(len(e_m) * i / len(e_M))
        #print(f'i={i}, idx_m={idx_m}')
        err += (e_M[i] - e_m[idx_m]) ** 2
    err /= len(e_M)

    return err
    

if len(sys.argv) > 3:
    print('No máximo duas respostas ao impulso suportadas')
    exit()

impulse_filenames = sys.argv[1:]

square = False
normalize_energy = True
smooth_factor = 1/200.0
#smooth_factor = 0
xlim = (0, 0.7)
colors = ['k', 'b']
#colors = [None, None]
alphas = [0.6, 0.5]

impulse_responses = []

# abre o arquivo com a resposta ao impulso

for i in range(len(impulse_filenames)):

    impulse_response_file = impulse_filenames[i]
    impulse_response = ImpulseResponse.load_from_npy_file(impulse_filenames[i])

    if square:
        impulse_response.energy_arr = impulse_response.energy_arr**2

    if smooth_factor > 0:
        a = len(impulse_response.energy_arr)
        ws = int((a/impulse_response.time_arr[-1]) * smooth_factor)
        print(f'smoothing impulse response with window size = {ws}')
        impulse_response.energy_arr = np.convolve(np.ones(ws), impulse_response.energy_arr)
        impulse_response.energy_arr = impulse_response.energy_arr[:a]

    if normalize_energy:
        impulse_response.energy_arr = impulse_response.energy_arr/np.max(impulse_response.energy_arr)

    impulse_responses.append(impulse_response)
    print(f'impulse_response_file: {impulse_filenames[i]}')
    print(f'impulse response size: {impulse_response.size()} ({impulse_response.size_in_seconds()}s)')
    print(f'impulse total energy: {impulse_response.energy()}')
    print(f'impulse mean amplitude: {np.mean(impulse_response.energy_arr)}')

# printa respostas ao impulso

for i in range(len(impulse_responses)):
    impulse_response = impulse_responses[i]
    plot_impulse_response(impulse_response.energy_arr, impulse_response.time_arr, xlim=xlim, show=False, color=colors[i], alpha=alphas[i])
    plt.legend(impulse_filenames, prop={'size': 8})   

plt.show()    

# plota curva de decaimento

for i in range(len(impulse_responses)):

    impulse_response = impulse_responses[i]
    decay_curve = np.zeros(len(impulse_response.energy_arr))
    decay_curve[0] = impulse_response.energy_arr.sum()

    for j in range(1, len(impulse_response.energy_arr)):
        decay_curve[j] = decay_curve[j-1] - impulse_response.energy_arr[j-1]
    decay_curve = decay_curve / decay_curve[0]
    
    impulse_response.decay_arr = decay_curve
    
    plt.title("Curva de decaimento")
    plt.xlabel("Tempo (s)")
    plt.ylabel("Amplitude")
    plt.plot(impulse_response.time_arr, impulse_response.decay_arr, color=colors[i])
    if xlim is not None:
        plt.xlim(xlim)

    plt.legend(impulse_filenames, prop={'size': 8})        

plt.show()

# calcula erro medio quadrático

if len(impulse_responses) == 2:

    mse = mean_squared_error(impulse_responses[0], impulse_responses[1], xlim)
    print(f'IMPULSE RESPONSE: MSE = {mse}, RMSE = {np.sqrt(mse)}')

    mse = mean_squared_error(impulse_responses[0], impulse_responses[1], xlim, decay=True)
    print(f'IMPULSE RESPONSE DECAY: MSE = {mse}, RMSE = {np.sqrt(mse)}')
