import numpy as np
import functions
import constants


class ImpulseResponse:

    def __init__(self, energy_arr, time_arr):
        self.energy_arr = energy_arr
        self.time_arr = time_arr

    def size(self):
        return len(self.time_arr)

    def size_in_seconds(self):
        return self.time_arr[-1]

    def energy(self):
        return np.sum(self.energy_arr)

    @staticmethod
    def load_from_elements(impulse_elements, elements_per_sec):

        # carrega resposta com base na lista de elementos

        arr_size = max(impulse_elements, key=lambda x:x[0])[0] + constants.fft_size * 2
        #print(f'arr_size={arr_size}')

        energy_arr = np.zeros(arr_size)

        #time_arr = np.arange(len(energy_arr))/elements_per_sec
        time_arr = np.arange(len(energy_arr))/elements_per_sec - constants.fft_size/elements_per_sec
        for e in impulse_elements:
            energy_arr[e[0]:e[0]+len(e[1])] += e[1]

        return ImpulseResponse(energy_arr, time_arr)

    @staticmethod
    def load_from_npy_file(filename):

        # carrega resposta ao impulso do arquivo

        impulse_response = np.load(filename)
        print(f'Impulse response loaded from {filename}')
        return ImpulseResponse(impulse_response[0], impulse_response[1])

    def save_to_npy_file(self, filename):

        # salva resposta ao impulso no arquivo

        np.save(filename, np.array([self.energy_arr, self.time_arr]))
        print(f'Impulse response saved to {filename}')

    def get_resampled_array(self, sample_rate):

        # transforma o tamanho da resposta ao impulso com base no sample_rate

        sampled_array = np.zeros(int(sample_rate * self.time_arr[-1])+1)
        for i in range(self.size()):
            sampled_array[int(sample_rate * self.time_arr[i])] = self.energy_arr[i]

        #k = len(sampled_array) / self.size()
        #for i in range(len(sampled_array)):
        #    sampled_array[i] = self.energy_arr[int(i/k)]

        return sampled_array

    def get_interp_resampled_array(self, sample_rate):

        # transforma o tamanho da resposta ao impulso com base no sample_rate, com interpolação linear

        time_arr_resampled = np.linspace(0, self.size_in_seconds(), int(sample_rate * self.size_in_seconds())+1)
        energy_arr_resampled = np.interp(time_arr_resampled, self.time_arr, self.energy_arr)

        total_energy_before = np.sum(np.abs(self.energy_arr))
        total_energy_after = np.sum(np.abs(energy_arr_resampled))
        #total_energy_before = np.sqrt(np.sum(self.energy_arr ** 2))
        #total_energy_after = np.sqrt(np.sum(energy_arr_resampled ** 2))
        #total_energy_before = np.sum(self.energy_arr ** 2)
        #total_energy_after = np.sum(energy_arr_resampled ** 2)

        print(f'total_energy_before: {total_energy_before}')
        print(f'total_energy_after: {total_energy_after}')

        energy_arr_resampled = energy_arr_resampled / (0.1*total_energy_after / total_energy_before)
        #energy_arr_resampled = energy_arr_resampled / np.sqrt(total_energy_after / total_energy_before)
        print(f'total_energy_after normalizing: {np.sum(np.abs(energy_arr_resampled))}')

        return energy_arr_resampled

    def get_time_index(self, time_in_s):
        return int(((time_in_s - self.time_arr[0]) / (self.time_arr[-1] - self.time_arr[0])) * len(self.energy_arr))


class Ray:

    def __init__(self, P, W, frequency_amplitude=None, initial_amplitude=None, initial_dist=0):

        self.P = P  # ponto inicial do raio
        self.W = functions.normalize(W)  # vetor indicando a direção do raio (deve ser normalizado !!)

        self.frequency_amplitude = frequency_amplitude
        if frequency_amplitude is None:
            self.frequency_amplitude = np.ones(constants.fft_size) * initial_amplitude

        self.initial_dist = initial_dist

    def mean_amplitude(self):
        return self.frequency_amplitude.mean()

    def __repr__(self):
        return f'<Ray: mean_amplitude={self.mean_amplitude()}, initial_dist={self.initial_dist}>'


class Triangle:

    def __init__(self, P0, P1, P2, referenced_object):

        self.P = P0  # ponto de referencia do triangulo

        # U e V nao devem ser normalizados !!!
        self.U = P1-P0  # vetor que indica direção e tamanho da primeira aresta do triangulo
        self.V = P2-P0  # vetor que indica direção e tamanho da segunda aresta do triangulo

        self.N = np.cross(self.U, self.V)
        self.N = functions.normalize(self.N)

        self.referenced_object = referenced_object  # referencia ao objeto que possui esse triangulo

    def calc_intersection(self, ray):

        # calcula interseção de um raio com este triangulo
        # retorna u,v,w
        # u é um escalar indicando a distancia entre o ponto de interseção e o ponto P deste triangulo, na direção do vetor U
        # v é um escalar indicando a distancia entre o ponto de interseção e o ponto P deste triangulo, na direção do vetor V
        # w é um escalar indicando a distancia entre o ponto de interseção e o ponto P do raio
        
        p0x = self.P[0]
        p0y = self.P[1]
        p0z = self.P[2]
        ux = self.U[0]
        uy = self.U[1]
        uz = self.U[2]
        vx = self.V[0]
        vy = self.V[1]
        vz = self.V[2]

        px = ray.P[0]
        py = ray.P[1]
        pz = ray.P[2]
        wx = ray.W[0]
        wy = ray.W[1]
        wz = ray.W[2]
        
        V = np.array([[ux, vx, -wx], [uy, vy, -wy], [uz, vz, -wz]])
        P = np.array([[px - p0x], [py - p0y], [pz - p0z]])
        
        try:
            K = np.dot(np.linalg.inv(V), P)

            u = K[0][0]
            v = K[1][0]
            w = K[2][0]
            if u >= 0 and v >= 0 and (u + v <= 1) and w > 0:
                return u,v,w
            else:
                return None
        except Exception:
            # matriz nao tem inversa, nao tem solucao
            return None
