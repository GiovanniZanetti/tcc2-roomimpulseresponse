def load_geometry_from_obj(obj_filename):

    # carrega arquivo .obj e le triangulos de cada objeto
    # retorna dicionario no formato <nome_objeto>: <lista triangulos e seus respectivos vertices>

    f = open(obj_filename, "r")

    vertices_list = []
    objects_dict = {}
    current_object = ""

    while True:
        line = f.readline()
        if not line:
            break

        line = line[:-1]  # tira o \n

        code = line[0]
        info = line[2:]

        #print(code)
        #print(info)

        if code == "o":
            current_object = info
            #print(f'current_object: {current_object}')
            objects_dict[current_object] = []

        if code == "v":
            vertices = info.split(" ")
            vertices = [float(v) for v in vertices]
            vertices_list.append(vertices)
            #print(vertices)

        if code == "f":
            indices = info.split(" ")
            indices = [int(i) for i in indices]
            vertices = list(map(lambda x: vertices_list[x-1], indices))
            #print(indices)
            #print(vertices)
            objects_dict[current_object].append(vertices)

    #print("objects_dict:")
    #print(objects_dict)

    #print("closing file")
    f.close()

    return objects_dict

